import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * created: Feb 21, 2015 9:07:16 PM
 */

/**
 * 
 * @author furunkel027@bitbucket.org
 * 
 */
public class Proov2 {

	public static final int MAX = 100000;

	/**
	 * @origin https://jpoial@bitbucket.org/i231/kodu2.git
	 * @author jpoial@bitbucket.org
	 */
	static public <T extends Object & Comparable<? super T>> void insertionSort(
			List<T> a, int l, int r) {
		if (a.size() < 2)
			return;
		if ((r - l) < 2)
			return;
		for (int i = l + 1; i < r; i++) {
			T b = a.remove(i);
			int j;
			for (j = l; j < i; j++) {
				if (b.compareTo(a.get(j)) < 0)
					break;
			}
			a.add(j, b); // insert b to position j
		}
	} // insertionSort()

	/**
	 * No suxxess with that. How to pass T as a parameter? Comparable key ?
	 *  ---- UNFINISHED ----
	 * @author jpoial@bitbucket.org (the frame)
	 * @author furunkel027@bitbucket.org (the filling)
	 * 
	 */
	static public <T extends Object & Comparable<? super T>> int BinarySearch(
			List<T> a, int low, int high, Comparable key) {
		int mid = 0;
		if (low == high)
			return low;
		mid = low + ((high - low) / 2); // keskel
		// DEBUG System.out.println("MID = " + mid);
		// T midValue = a.get(mid);
		// T keyValue = key;

		// T x = a.get (low + (high - low) / 2);

		// do {

		// if (midValue.compareTo(keyValue) < 0) {break};

		//
		// if (a.compareTo((Integer)key) < 0) break;
		// if (key > a[mid])

		// return BinarySearch (a, mid + 1, high, key);
		// else if (key < a[mid])
		// return BinarySearch (a, low, mid, key);
		//
		// return mid;
		// }

		return mid;
	}

	
	static public <T extends Object & Comparable<? super T>> void swap(
			List<T> a, int source, int target) {
		
		
		
	}
	/**
	 * 
	 * @author furunkel027@bitbucket.org
	 * 
	 */
	static public <T extends Object & Comparable<? super T>> void binaryInsertionSort(
			List<T> maatriks, int l, int r) {
		// Nagu alati, KOMMENTAARID JA ÕIENDUSED:
		// Algseks eeskujuks see näide siin:
		// - http://www.brpreiss.com/books/opus4/html/page491.html
		// teised mitu on liiga trikilised, et aru saada &
		// kahjuks ei osanud super T tüüpi parameetrit
		// eraldi alam-meetodile ka edastada - see piiras
		// eeskujud neile, kus asi ennast rekursiivselt
		// välja ei kutsu.

		// Erijuhud - aga testides saab ka ilma nendeta hakkama.
		// if ((maatriks.size() < 2) && ((r - l) < 2)) return;

		// iga elemendiga mis vasakult alustades jalge ette satub
		for (int i = l + 1; i < r; i++) {

			// Jee! Boundary sai korda kui left = l and != 0
			int left = l;
			int right = i;
			// Vaja on argumendi väärtust maatriksi sel kohal
			Comparable t = maatriks.get(i);
			// Ja siit edasi on küssa, kuhu täpselt see PISTA.

			// Siitmaalt binaarotsing kuniks l!=r
			while (left < right) {
				int middle = ((left + right) / 2);
				
				// Divide & conquer
				// Jee! ">=" vs "=" tegi stability korda!!!
				if (t.compareTo(maatriks.get(middle)) >= 0) {
					left = middle + 1;
				} else {
					right = middle;
				} // kui vasak ja parem äär koos, siis koht õige
			} // endWHILE
			for (int j = i; j > left; j--) { // --j j-- no difference
				T temporal = maatriks.get(j - 1);
				maatriks.set(j - 1, maatriks.get(j));
				maatriks.set(j, temporal);
				// because
				// T b = maatriks.remove(j - 1);
				// maatriks.add(j, b);
				// was REALLY SLOW.

			}
		} // endFOR
	} // binaryInsertionSort()

	/**
	 * @author furunkel027@bitbucket.org
	 * @param args
	 */
	public static void main(String[] args) {
		long startTime = new Date().getTime();
		System.out.println("START -- " + startTime);
		List<Integer> randlist = new ArrayList<Integer>(MAX); // original
		Random generaator = new Random();
		int maxKey = Math.min(1000, (MAX + 32) / 16);
		for (int i = 0; i < MAX; i++) {
			randlist.add(new Integer(generaator.nextInt(maxKey)));
		}

		// List<Integer> randlist = new ArrayList<Integer>(Arrays.asList (5, 3,
		// 2, 1, 5, 2));
		// DEBUG System.out.println(randlist);

		// int rightLimit = randlist.size() / 3;
		// int rightLimit = MAX;
		int leftLimit = 0;
		int rightLimit = MAX;

		System.out.println("RightLimit = " + rightLimit);
		System.out.println("leftLimit = " + leftLimit);
		List<Integer> copy1 = new ArrayList<Integer>(randlist);

		System.out.println();
		binaryInsertionSort(copy1, leftLimit, rightLimit);
		// DEBUG System.out.println(copy1);
		System.out.println();

		long stopTime = new Date().getTime();
		System.out.println("STOP  -- " + startTime);
		System.out.println("Värk kestis ajaliselt: " + (stopTime - startTime)
				+ " ms");
	}

}
