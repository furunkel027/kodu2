import java.util.List;
import java.util.ArrayList;

/**
 * created: Mar 15, 2015 12:20:53 PM
 */

/**
 * @author furunkel0277@bitbucket.ee
 */
public class Proov6 {
	// ================================================================
	static public <T extends Object & Comparable<? super T>> void swap(
			List<T> x, int seller, int buyer) {
		T temporal = x.get(seller);
		x.set(seller, x.get(buyer));
		x.set(buyer, temporal);
	}
	// ================================================================
	static public <T extends Object & Comparable<? super T>> void shift(
			List<T> x, int aBoundary, int bBoundary) {
		T temporal = x.get(bBoundary);
		for (int i = bBoundary; i > aBoundary; i--) {
			// System.out.print(x);
			// System.out.println();
			// System.out.printf(" - vahetame omavahel kohad i[%2d] ja i[%2d].%n",
			// 		i - 1, i);
			swap(x, i - 1, i);
			// System.out.println(x);
			// System.out.println();
		}
		x.set(aBoundary, temporal); // tagant võetud element ette
	}
	// ================================================================
	static public <T extends Object & Comparable<? super T>> int linearSearch(
			List<T> a, int low, int high, Comparable keyValue) {
		System.out.printf(
				"LINEARSEARCH ---  Left = %3d | Right = %3d | keyValue=%3d%n",
				low, high, keyValue);
		boolean edge = false;

		for (int i = high; i >= low; i--) {
			int suurus = a.size();
			Comparable kontrollitav = a.get(i);

			// System.out
			// 		.printf("\t Size = %2d | I = %2d | kontrollitav = %2d | keyValue = %3d%n",
			// 				suurus, i, kontrollitav, keyValue);
			if (i == low)
				edge = true;

			if ((keyValue.compareTo(kontrollitav) >= 0)) {
				// System.out.println("\t i=" + i);
				return i;
			}	
			if (edge) return low;
		}
		return 666; // Miski on perses
	}

	
	
	// ================================================================
//	static public <T extends Object & Comparable<? super T>> int binarySearch(
//			List<T> a, int low, int high, Comparable keyValue) {
////		System.out.printf(
////				"LINEARSEARCH ---  Left = %3d | Right = %3d | keyValue=%3d%n",
////				low, high, keyValue);
//		boolean edge = false;
//
//		for (int i = high; i >= low; i--) {
//			int suurus = a.size();
//			Comparable kontrollitav = a.get(i);
//			if (i == low)
//				edge = true;
//
//			if ((keyValue.compareTo(kontrollitav) >= 0)) {
//				// System.out.println("\t i=" + i);
//				return i;
//			}
//		}
//		return low;
//	}
	
	
	
	static public <T extends Object & Comparable<? super T>> void binaryInsertionSort(
			List<T> m, int l, int r) {
		// http://en.wikipedia.org/wiki/Insertion_sort 
		int suurus = m.size();
		System.out.printf("INSERTIONSORT --- Size = %3d | Left = %3d | Right = %3d%n%n", suurus, l, r);
		for (int i = (l+1); i<=r; i++) {
			boolean vahetada = false;
			System.out.printf("i = %3d%n", i);
			Comparable kontrollitav = m.get(i);
			Comparable eelseisev = m.get(i-1);
			if ((kontrollitav.compareTo(eelseisev)) <= 0) {
				vahetada = true;
			}
			
			if (vahetada) {
				// Põhiküsimus nüüd, et kuhu vahetada
				int vaheJoon=i; // j saab liikuda vaid vahekohast ettepoole
				// int kuhuKohalePista = linearSearch (m, l, vaheJoon-1, kontrollitav);
				
				System.out.println(m);
				int startOfShift = linearSearch(m, 0, vaheJoon-1, kontrollitav);
				System.out.printf("i = %3d | pre = %3d | now = %3d | exchange? = %5s | toWhatPos = %3d%n",
						i, eelseisev, kontrollitav, vahetada, startOfShift);
				shift(m,startOfShift+1,vaheJoon);
				System.out.println(m);
				System.out.println();
				
				
//				// number, mis asub kohal [i] peab sattuma kohale [kuhupista]
//				// aga kõik vahepealsed nihkuvad samuti paremale
//			for (int k = vaheJoon; k > kuhuKohalePista; k--) {
//				System.out.print(m);
//				System.out.printf(" - vahetame omavahel kohad i[%2d] ja i[%2d].%n", k-1, k);
//				swap(m, k - 1, k);
//				System.out.println(m);
//				System.out.println();
//				
//			}

			
				
				
			}
		}
	
	}
	
	
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int kogus = 16;
		List<Integer> poolsorditud = new ArrayList<Integer>(kogus); // original
		poolsorditud.add(1);
		poolsorditud.add(2);
		poolsorditud.add(3);
		poolsorditud.add(4);
		poolsorditud.add(5);
		poolsorditud.add(6);
		poolsorditud.add(7);
		poolsorditud.add(8);
		poolsorditud.add(9);
		poolsorditud.add(0);
		poolsorditud.add(2);
		poolsorditud.add(3);
		poolsorditud.add(1);
		poolsorditud.add(0);
		poolsorditud.add(4);
		poolsorditud.add(8);
		System.out.println(poolsorditud);
		System.out.println();



//		int target = 9;
//		System.out.println("MAIN: target = " + target + " | Value = "
//				+ poolsorditud.get(target));
//		int startOfShift = linearSearch(poolsorditud, 0, target-1,
//				poolsorditud.get(target));
//		System.out.println("SeeNumberPärastMidaPistetakse: " + (startOfShift));
//		
//		System.out.println("Vahetustsükli ulatus (inclusive)= " + (target-startOfShift));
//		shift(poolsorditud,startOfShift+1,target);
//		System.out.println();
//		System.out.println(poolsorditud);
		System.out.println("=========================================================");
		System.out.println();
		binaryInsertionSort(poolsorditud, 0, kogus-1);
	}

}
