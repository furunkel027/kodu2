import java.util.List;
import java.util.ArrayList;

/**
 * created: Mar 15, 2015 3:49:57 PM
 */

/**
 * @author furunkel027@bitbucket.com
 */
public class Proov7 {

	/**
	 * The method exchanges 2 values in a list under manipulation.
	 * 
	 * @param x
	 *            - List under manipulation
	 * @param - seller donor location
	 * @param - buyer receiver location
	 */
	static public <T extends Object & Comparable<? super T>> void swap(
			List<T> x, int seller, int buyer) {
		T temporal = x.get(seller);
		x.set(seller, x.get(buyer));
		x.set(buyer, temporal);
	}

	/**
	 * The method groups single swaps more conveniently into a group.
	 * 
	 * @param x
	 *            - List under manipulation
	 * @param aBoundary
	 *            - utmost left location
	 * @param bBoundary
	 *            - utmost right location, with the movable element
	 */
	static public <T extends Object & Comparable<? super T>> void shift(
			List<T> x, int aBoundary, int bBoundary) {
		T temporal = x.get(bBoundary);
		for (int i = bBoundary; i > aBoundary; i--)
			swap(x, i - 1, i);
		x.set(aBoundary, temporal); // tagant võetud element pannakse ette
	}

	/**
	 * The method accomplishes linear search. Obsolete. For reference only.
	 * 
	 * @param a
	 *            - the list that we want to manipulate
	 * @param low
	 *            - lower boundary
	 * @param high
	 *            - higher boundary
	 * @param keyValue
	 *            - the comparable value we seek a cell for
	 * @return
	 */
	static public <T extends Object & Comparable<? super T>> int linearSearch(
			List<T> a, int low, int high, Comparable keyValue) {
		boolean edge = false;
		// Tuleme ülalt alla, kas leiame väiksema?
		for (int i = high; i >= low; i--) {
			if (i == low)
				edge = true; // => oluline allpool
			Comparable kontrollitav = a.get(i);
			if ((kontrollitav.compareTo(keyValue) < 0)) {
				return i + 1;
			} // Leitud, aga tagastatame eelmise (suurema) väärtuse
		} // FOR
		if (edge)
			return low; // sellest väiksemat ei leidnudki
		return 666; // Debugging aid - Ei tohiks juhtuda
	}

	/**
	 * Meetod teeb binaarotsingut, tagastab listi indeksi sihukese väärtuse,
	 * kuhu sortimisalgoritm võiks piste vahele teha.
	 * 
	 * @param a
	 *            - list, mida parasjagu manipuleerime
	 * @param low
	 *            - otsingu alumine raja
	 * @param high
	 *            - otsingu ülemine raja
	 * @param keyValue
	 *            - etteantud väärtus, millele tegelt pistmiskohta otsime
	 * @return - tagastab massiivile positsioni, kuhu oleks sobilik keyValue
	 *         vahele pista.
	 */
	static public <T extends Object & Comparable<? super T>> int binarySearch(
			List<T> a, int low, int high, Comparable keyValue) {

		if (low > high)
			return low; // Erijuhtum, läbi sai.
		int mid = low + (high - low) / 2; // umbes keskpaik
		int signValue = (keyValue.compareTo(a.get(mid)));
		if (signValue < 0)
			return binarySearch(a, low, mid - 1, keyValue);
		if (signValue > 0)
			return binarySearch(a, mid + 1, high, keyValue);
		else
			return mid;
	}

/**
 * Insertion Sort (Generic, depends on external Search facility)
 * @param m - list that we currently manipulate
 * @param l - leftmost boundary
 * @param r - rightmost boundary
 */
	static public <T extends Object & Comparable<? super T>> void binaryInsertionSort(
			List<T> m, int l, int r) {
		for (int i = (l + 1); i <= r; i++) {
			boolean weDoSwap = false;
			Comparable kontrollitav = m.get(i);
			Comparable eelseisev = m.get(i - 1);
			if ((kontrollitav.compareTo(eelseisev)) <= 0) { // Väiksem
				weDoSwap = true;
			}

			if (weDoSwap) {
				int kuhuMaani = i; // saab liikuda vaid vahekohast ettepoole
				int kustSaadik = binarySearch(m, l, kuhuMaani - 1, kontrollitav);
				shift(m, kustSaadik, kuhuMaani);
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Testimiseks

		int kogus = 16;

		// ===================================================

		List<Integer> sortimata = new ArrayList<Integer>(kogus); // original
		sortimata.add(71);
		sortimata.add(1);
		sortimata.add(5);
		sortimata.add(8);
		sortimata.add(0);
		sortimata.add(3);
		sortimata.add(0);
		sortimata.add(-15);
		sortimata.add(4);
		sortimata.add(2);
		sortimata.add(-5);
		sortimata.add(1);
		sortimata.add(0);
		sortimata.add(6);
		sortimata.add(2);
		sortimata.add(-10);

		// System.out.println(sortimata);

		List<Integer> poolsorditud = new ArrayList<Integer>(kogus); // original
		poolsorditud.add(1);
		poolsorditud.add(2);
		poolsorditud.add(3);
		poolsorditud.add(4);
		poolsorditud.add(5);
		poolsorditud.add(6);
		poolsorditud.add(7);
		poolsorditud.add(8);
		poolsorditud.add(9);
		poolsorditud.add(0);
		poolsorditud.add(2);
		poolsorditud.add(3);
		poolsorditud.add(1);
		poolsorditud.add(0);
		poolsorditud.add(4);
		poolsorditud.add(8);
		// System.out.println(poolsorditud);

		List<Integer> test = sortimata;
		System.out.println();

		// int vahemuutuja;
		// int pos = 9;
		// Comparable cmp = test.get(pos);
		// // vahemuutuja = linearSearch( test, 0, pos-1, cmp);
		// //
		// // System.out.println("LinearSearch Vahemuutuja = " + vahemuutuja);
		// // shift(test,vahemuutuja,pos);
		// //
		// System.out.println("========end of shift =================================================");
		// // System.out.println();
		//
		//
		// vahemuutuja = binarySearch( test, 0, 9, cmp);
		// System.out.println();
		// System.out.println("binarySearch Vahemuutuja = " + vahemuutuja);
		// System.out.println();
		// shift(test,vahemuutuja,9);

		System.out.println(test);
		binaryInsertionSort(test, 0, 15);

		// Comparable a = poolsorditud.get(15);
		// Comparable b = poolsorditud.get(14);
		// int signValue = (a.compareTo(b));
		// System.out.println("A compare to B: " + signValue);
		// signValue = (b.compareTo(a));
		// System.out.println("B compare to A: " + signValue);

		System.out.println();
		System.out.println(test);
		System.out
				.println("=========END ================================================");
		System.out.println();

	}

}
