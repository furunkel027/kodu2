import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * created: Feb 21, 2015 9:07:16 PM 
 */

/**
 * 
 * @author furunkel027@bitbucket.org
 * 
 */
public class Proov3 {

	public static final int MAX = 39;


	/**
	 * No suxxess with that. How to pass T as a parameter? Comparable key ? ----
	 * UNFINISHED ----
	 * 
	 * @author jpoial@bitbucket.org (the frame)
	 * @author furunkel027@bitbucket.org (the filling)
	 * 
	 */
	static public <T extends Object & Comparable<? super T>> int BinarySearch(
			List<T> a, int low, int high, Comparable keyValue) {
		int mid = 0;
		if (low == high)
			return low;
		mid = low + ((high - low) / 2); // keskel
		System.out.println("MID = " + mid);
		Comparable midValue = a.get(mid);
		T x = a.get(mid);

		while (true) {

			if (midValue.compareTo(keyValue) < 0)
				break;

			if ((keyValue.compareTo(x) >= 0)) {

				return BinarySearch(a, mid + 1, high, keyValue);
			} else {
				if ((keyValue.compareTo(x) > 0)) {
					return BinarySearch(a, low, mid, keyValue);
				}
			}
		}
		return mid;
	}

	/**
	 * 
	 * @author furunkel027@bitbucket.org
	 * 
	 */
	static public <T extends Object & Comparable<? super T>> void binaryInsertionSort(
			List<T> maatriks, int l, int r) {
		// iga elemendiga mis vasakult alustades jalge ette satub
		for (int i = l + 1; i < r; i++) {
			int left = l;
			int right = i;
			// Vaja on argumendi väärtust maatriksi sel kohal
			Comparable t = maatriks.get(i);
			// Ja siit edasi on küssa, kuhu täpselt see PISTA.

			// binarySearch (maatriks, l, r, maatriks.get(i)); // MIKS EI TÖÖTA?
			
			// Binaarotsing kestab, kuniks l!=r
			while (left < right) {
				int middle = ((left + right) / 2);

				// Divide & conquer
				if (t.compareTo(maatriks.get(middle)) >= 0) {
					left = middle + 1;
				} else {
					right = middle;
				} // kui vasak ja parem äär koos, siis koht õige
			} // endWHILE

				// Nüüd algab lõputu asendamine
			for (int j = i; j > left; j--) {
				// System.out.println(pott);
				swap(maatriks, j - 1, j);
				// System.out.println(pott);
				// System.out.println();
			}
		} // endFOR
	} // binaryInsertionSort()

	static public <T extends Object & Comparable<? super T>> void swap(
			List<T> pott, int seller, int buyer) {
		T temporal = pott.get(seller);
		pott.set(seller, pott.get(buyer));
		pott.set(buyer, temporal);
	}

	/**
	 * @author furunkel027@bitbucket.org
	 * @param args
	 */
	public static void main(String[] args) {
		long startTime = new Date().getTime();
		System.out.println("START -- " + startTime);
		List<Integer> randlist = new ArrayList<Integer>(MAX); // original
		Random generaator = new Random();
		int maxKey = Math.min(1000, (MAX + 32) / 16);
		for (int i = 0; i < MAX; i++) {
			randlist.add(new Integer(generaator.nextInt(maxKey)));
		}
		System.out.println(randlist);

		int leftLimit = 0;
		int rightLimit = MAX;

		System.out.println("RightLimit = " + rightLimit);
		System.out.println("leftLimit = " + leftLimit);
		List<Integer> copy1 = new ArrayList<Integer>(randlist);

		System.out.println();
		binaryInsertionSort(copy1, leftLimit, rightLimit);
		System.out.println(copy1);
		System.out.println();

		long stopTime = new Date().getTime();
		System.out.println("STOP  -- " + startTime);
		System.out.println("Värk kestis ajaliselt: " + (stopTime - startTime)
				+ " ms");
	}

}
