import java.util.ArrayList;
import java.util.List;

/**
 * created: Mar 15, 2015 12:20:53 PM
 */

/**
 * @author furunkel0277@bitbucket.ee
 */
public class Proov5 {

	
	static public <T extends Object & Comparable<? super T>> int linearSearch(
			List<T> a, int low, int high, Comparable keyValue) {
		System.out.printf("LINEARSEARCH ---  Left = %3d | Right = %3d | keyValue=%3d%n", low, high, keyValue);
		boolean edge = false;

		for (int i = high; i>=low; i--) {
			int suurus = a.size();
			Comparable kontrollitav = a.get(i);

			System.out.printf("\t Size = %2d | I = %2d | kontrollitav = %2d | keyValue = %3d%n", suurus, i, kontrollitav, keyValue);
			if (i == low) edge = true ; 
			if ((kontrollitav.compareTo(keyValue) <= 0) || edge) {
			System.out.println("\t FOUND!!! = " + i);
			return i;
		} 
		}
		return -1; // Muud ei jää üle kui esimese ette
	}

	
	static public <T extends Object & Comparable<? super T>> void swap(
			List<T> x, int seller, int buyer) {
		T temporal = x.get(seller);
		x.set(seller, x.get(buyer));
		x.set(buyer, temporal);
	}
	
	
	
	static public <T extends Object & Comparable<? super T>> void binaryInsertionSort(
			List<T> m, int l, int r) {
		// http://en.wikipedia.org/wiki/Insertion_sort 
		int suurus = m.size();
		
		System.out.printf("INSERTIONSORT --- Size = %3d | Left = %3d | Right = %3d%n%n", suurus, l, r);
		
		for (int i = (l+1); i<=r; i++) {
			boolean vahetada = false;
			// System.out.printf("i = %3d%n", i);
			Comparable kontrollitav = m.get(i);
			Comparable eelseisev = m.get(i-1);
			// if (keyValue.compareTo(kontrollitav) >= 0) {
			if ((kontrollitav.compareTo(eelseisev)) <= 0) {
				vahetada = true;
			}
			
			
			if (vahetada) {
				// Põhiküsimus nüüd, et kuhu vahetada
				int vaheJoon=i; // j saab liikuda vaid vahekohast ettepoole
				int kuhuKohalePista = linearSearch (m, l, vaheJoon-1, kontrollitav);
				System.out.printf("i = %3d | pre = %3d | now = %3d | exchange? = %5s | toWhatPos = %3d%n", i, eelseisev, kontrollitav, vahetada, kuhuKohalePista);

				// number, mis asub kohal [i] peab sattuma kohale [kuhupista]
				// aga kõik vahepealsed nihkuvad samuti paremale
			for (int k = vaheJoon; k > kuhuKohalePista; k--) {
				System.out.print(m);
				System.out.printf(" - vahetame omavahel kohad i[%2d] ja i[%2d].%n", k-1, k);
				swap(m, k - 1, k);
				System.out.println(m);
				System.out.println();
				
			}
			
			}
		}
	
	}
	
	
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Testitakse siin.
		int kogus = 16;

//		List<Integer> poolsorditud = new ArrayList<Integer>(kogus); // original
//		poolsorditud.add(1);
//		poolsorditud.add(2);
//		poolsorditud.add(3);
//		poolsorditud.add(4);
//		poolsorditud.add(5);	
//		poolsorditud.add(6);
//		poolsorditud.add(7);
//		poolsorditud.add(8);
//		poolsorditud.add(9);
//		poolsorditud.add(4);
//		poolsorditud.add(2);	
//		poolsorditud.add(3);
//		poolsorditud.add(1);
//		poolsorditud.add(0);
//		poolsorditud.add(4);
//		poolsorditud.add(8);
//		System.out.println(poolsorditud);
//		
//		int pos = 11;
//		System.out.print("Vahetatav positsioon: " + pos);
//		Comparable  vahetatav = poolsorditud.get(pos);
//		System.out.println("  Väärtus: " + vahetatav);
//		
//		int pistekoht = linearSearch (poolsorditud, 0, 8, vahetatav);
//		System.out.println("  Pistekoht: " + pistekoht); // mille ette
		
		
		// ------------------------
		System.out.println();
		System.out.println("================================================");
		List<Integer> suva = new ArrayList<Integer>(kogus); // original
		
		suva.add(4);
		suva.add(7);
		suva.add(8);
		suva.add(0);
		suva.add(3);	
		suva.add(5);
		suva.add(6);
		suva.add(7);
		suva.add(8);
		suva.add(9);
		suva.add(4);	
		suva.add(1);
		suva.add(9);
		suva.add(7);
		suva.add(4);
		suva.add(8);
		System.out.println(suva);
		
		int positsioon = kogus-1;
		System.out.println("Positsioon: " + positsioon);
		Comparable testkoht = suva.get(positsioon);
		System.out.println("Väärtus: " + testkoht);
		

		
		int leftLimit = 0;
		int rightLimit = positsioon;
		binaryInsertionSort(suva, leftLimit, rightLimit);
		
		
	
	}

}
