import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * created: March 14, 2015 10:10:16 PM
 * Idee: lõpuks ometi saada BinaryInsertionSort tööle
 * korralikult rekursiivsena. 
 */

/**
 * 
 * @author furunkel027@bitbucket.org
 * 
 */
public class Proov4 {

	public static final int MAX = 17;


	/**
	 * @author jpoial@bitbucket.org (the frame)
	 * @author furunkel027@bitbucket.org (the filling)
	 * 
	 */
	static public <T extends Object & Comparable<? super T>> int linearSearch(
			List<T> a, int low, int high, Comparable keyValue) {
		boolean sobib = false;

		for (int i = high; i>=low; i--) {
			int suurus = a.size();
			System.out.printf("\t Size = %5d | I = %5d%n", suurus, i);
			Comparable kontrollitav = a.get(i);
			System.out.printf("\t kontrollitav = %3d | keyValue = %3d%n", kontrollitav, keyValue);
		if (keyValue.compareTo(kontrollitav) >= 0) {
			System.out.println("\t FOUND!!!");
			return i;
		} 
		}
		return low; // Muud ei jää üle kui esimese ette
	}

	/**
	 * 
	 * @author furunkel027@bitbucket.org
	 * 
	 */
	static public <T extends Object & Comparable<? super T>> void binaryInsertionSort(
			List<T> maatriks, int l, int r) {
		int suurus = 0;
		suurus = maatriks.size();
		System.out.printf("Size = %3d | Left = %3d | Right = %3d%n", suurus, l, r);
		// System.out.println();
		// Alates teisest elemendist, sest algus loksub paika niikunii
		for (int i = l + 1; i < r; i++) {
			int left = l;
			int right = i;
			// Kohal [i] hoitavat väärtust on vaja võrrelda, salvestame
			Comparable vahetatav = maatriks.get(i);
			System.out.println("Väärtus kohal i=" + i + " on " + vahetatav);
			// Edasine mure, kuhu täpselt see vahetada.
			// Esimene ots maatriksit on juba sorditud

			int kuhuEttePista = linearSearch (maatriks, left, right, vahetatav);

				// number, mis asub kohal [i] peab sattuma kohale [kuhupista]
				// ja kõik vahepealsed nihkuvad samuti paremale
			for (int j = i; j > left; j--) {
				System.out.println(maatriks);
				swap(maatriks, j - 1, j);
				// System.out.println(maatriks);
				// System.out.println();
			}
		} // endFOR
	} // binaryInsertionSort()


	
	static public <T extends Object & Comparable<? super T>> void swap(
			List<T> x, int seller, int buyer) {
		T temporal = x.get(seller);
		x.set(seller, x.get(buyer));
		x.set(buyer, temporal);
	}

	/**
	 * @author furunkel027@bitbucket.org
	 * @param args
	 */
	public static void main(String[] args) {
		long startTime = new Date().getTime();
		System.out.println("START -- " + startTime);
		List<Integer> suva = new ArrayList<Integer>(MAX); // original
		
		suva.add(4);
		suva.add(7);
		suva.add(8);
		suva.add(0);
		suva.add(3);	
		suva.add(5);
		suva.add(6);
		suva.add(7);
		suva.add(8);
		suva.add(9);
		suva.add(4);	
		suva.add(1);
		suva.add(9);
		suva.add(7);
		suva.add(4);
		suva.add(8);
		System.out.println(suva);
		
		int positsioon = 15;
		System.out.println("Positsioon: " + positsioon);
		Comparable testkoht = suva.get(positsioon);
		System.out.println("Väärtus: " + testkoht);
		
//		int vahetuskoht = linearSearch(suva,1,9,testkoht);
//		System.out.println("Vahetuskoha positsioon: " + vahetuskoht);
			// ------------------------
		
		int leftLimit = 0;
		int rightLimit = MAX;
		binaryInsertionSort(suva, leftLimit, positsioon);
			}

}
