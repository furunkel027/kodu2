import java.util.List;
import java.util.ArrayList;

	// Alustame siis kolmandat tööpäevast seeriat.
	// Mõttetu see asi ju on, aga tahaks teada, milles
	// seisnevad peakuju iseärasused
	

/**
 * created: Mar 15, 2015 9:20:24 PM
 */

/**
* @author furunkel027@bitbucket.com
*/
public class Proov8 {
	  
// http://stackoverflow.com/questions/3075752/binary-insertion-sort-algorithm
	
	  public static <T extends Object & Comparable<? super T>>
	    void binaryInsertionSort (List<T> a, int l, int r) {
	 
		  for (int i = l; i < r; i++){
			 Comparable temp = a.get(i);
			 int left = l;
	         int right = i;
	         while (left<right){
	                int middle=(left+right)/2;
	                // (temp >= a[middle])
	                if ((temp.compareTo(a.get(middle)) >= 0))
	                    left = middle + 1;
	                else
	                    right = middle;
	            }
	         for (int j=i;j>left;j--){
	                swap(a,j-1,j);
		  }
		  } 
		      
	   } // binaryInsertionSort()
	  
		static public <T extends Object & Comparable<? super T>> void swap(
				List<T> x, int seller, int buyer) {
			T temporal = x.get(seller);
			x.set(seller, x.get(buyer));
			x.set(buyer, temporal);
		}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// (http://stackoverflow.com/questions/363681/generating-random-integers-in-a-range-with-java)
		final int LSIZE = 17;
		ArrayList<Integer> randList = new ArrayList<Integer>(LSIZE);
		int minimum = -20;
		int maximum = 80;
		for (int i = 0; i < LSIZE; i++) {
			int lambist = minimum
					+ (int) (Math.random() * ((maximum - minimum) + 1));
			randList.add(new Integer(lambist));
		} 
		System.out.println(randList);
		int suurus = randList.size();
		int middle = (0 + (suurus - 0 - 1)/ 2);
		System.out.println("Middle: " + middle);
		binaryInsertionSort (randList, 0, suurus);
		
		System.out.println("------------------------------");
		
		System.out.println(randList);
		
//        int a[]=new int[]{12,10,34,23,9,7,8,5,6};
//        for (int i=0;i<a.length;i++){
//            System.out.print(a[i] + " ");
//        }
//        System.out.println();
//        
//        sort(a);
//        
//        for (int i=0;i<a.length;i++){
//            System.out.print(a[i] + " ");
//        }
//        System.out.println();
	}

}
